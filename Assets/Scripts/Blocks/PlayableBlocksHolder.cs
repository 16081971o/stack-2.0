﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Класс содержаший в себе ссылки на текущие активные блоки
/// </summary>

//TODO: Плохая релизация, нужно исправить
public class PlayableBlocksHolder
{
    // ссылка на движуйщийся блок
    public IDynamicBlock DynamicBlock;
    // ссылка на блок-платформу
    public IStaticBlock StaticBlock;
    // ссылка на падающий блок
    public IFallingBlock FallingBlock;
    public MonoBehaviour GetPlayableDynamicBlock()
    {
        return DynamicBlock as MonoBehaviour;
    }
    public MonoBehaviour GetPlayableStaticBlock()
    {
        return StaticBlock as MonoBehaviour;
    }
    public MonoBehaviour GetPlayableFallingBlock()
    {
        return FallingBlock as MonoBehaviour;
    }

    public List<IStaticBlock> StaticBlocks = new List<IStaticBlock>();
}
