﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FallingBlock : MonoBehaviour, IFallingBlock
{
    private Vector3 size { set { transform.localScale = value; } }
    private Vector3 position { set { transform.position = value; } }
    
    [Inject]
    public void Setup(Vector3 size, Vector3 position)
    {
        this.size = size;
        this.position = position;
    }
    private void Start()
    {
        gameObject.AddComponent<Rigidbody>();
    }
    public class Factory : PlaceholderFactory<Vector3,Vector3,FallingBlock> {}
}
