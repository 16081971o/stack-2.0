﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlatformBlock : MonoBehaviour, IStaticBlock
{
    private Vector3 size { set { transform.localScale = value; } }
    private Vector3 position { set { transform.position = value; } }
    private void Start()
    {
        gameObject.AddComponent<Rigidbody>();
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
    }
    [Inject]
    public void Setup(Vector3 size, Vector3 position)
    {

        this.size = size;
        this.position = position;
    }
    public class Factory : PlaceholderFactory<Vector3,Vector3,PlatformBlock> { }
}
