﻿using UnityEngine;
using Zenject;
public class Block : MonoBehaviour, IDynamicBlock
{
    private Vector3 size { set { transform.localScale = value; } }
    private Vector3 position { set { transform.position = value; } }
    
    [Inject]
    public void Setup(Vector3 size, Vector3 position)
    {
        this.size = size;
        this.position = position;
    }
    public class Factory : PlaceholderFactory<Vector3,Vector3,Block> { }
}
