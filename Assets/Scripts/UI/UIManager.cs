﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
/// <summary>
/// Отвечает за работу UI элементов
/// </summary>
public class UIManager : MonoBehaviour,IDisposable
{
   [SerializeField]
   private Animator MenuAnimator;
   
   [SerializeField]
   private Animator GameplayAnimator;
   
   [SerializeField]
   private Text Score;
   
   [SerializeField]
   private Text BestScore;
   
   [Inject] 
   private SignalBus _signalBus;
   
   [Inject] 
   private PlayerScore _playerScore;

   [SerializeField]
   private Text _tapText;
   
   public void Start()
   {
      _signalBus?.Subscribe<UpdateScoreSignal>(UpdateScore);
      BestScore.text = _playerScore.BestScore.ToString();
   }
   public void Dispose()
   {
      _signalBus?.TryUnsubscribe<UpdateScoreSignal>(UpdateScore);
   }
   /// <summary>
   /// Обновляем игровой счет
   /// </summary>
   private void UpdateScore()
   {
      Score.text     = _playerScore.Score.ToString();
      BestScore.text = _playerScore.BestScore.ToString();
   }
   /// <summary>
   /// Показываем/скрываем  интерфейс меню
   /// </summary>
   public void ShowMenu(bool show)
   {
      MenuAnimator.SetBool("Visible", show);
   }

   public void ChangeTitle(string title)
   {
      _tapText.text = title;
   }
   /// <summary>
   /// Показываем/скрываем игровое интерфейс
   /// </summary>
   public void ShowGameplay(bool show)
   {
      GameplayAnimator.SetBool("Visible", show);
   }
   
}
