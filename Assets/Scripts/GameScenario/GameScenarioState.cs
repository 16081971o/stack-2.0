﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameScenarioState
{
    /// порядок выполняния состояния
    public int order;
    
    //Колбейк о выполнении состояния
    public delegate void OnCompleteState();
    
    /// Запукается в начале состояния
    public abstract void Start(OnCompleteState onComplete);
    

    /// Работает каждый фрейм
    public abstract void Update();
  
    /// Срабатывает в конце работы состояния
    public abstract void End();

}
