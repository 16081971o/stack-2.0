﻿using System;
using UnityEngine;
using Zenject;
/// <summary>
/// Хранит в себе счет игрока, и обновляет его количество
/// </summary>
public class PlayerScore : IInitializable, IDisposable
{
    public int Score;
    public int BestScore;

    [Inject] 
    private SignalBus _signal;
    
    public void Initialize()
    {
        _signal.Subscribe<UpdateScoreSignal>(UpdateScore);
        _signal.Subscribe<SavePlayerScoreSignal>(SaveScore);
        GetScore();
    }

    public void GetScore()
    {
        bool noScore = !PlayerPrefs.HasKey("BestScore");
        
        if (noScore)
        {
            PlayerPrefs.SetInt("BestScore", 0);
            PlayerPrefs.Save();
        }
        BestScore = PlayerPrefs.GetInt("BestScore");
    }
    public void SaveScore()
    {
        if (Score >= BestScore)
        {
            PlayerPrefs.SetInt("BestScore", Score);
        }
    }
    public void UpdateScore()
    {
        Score++;

        if (Score > BestScore)
        {
            BestScore = Score;
        }
    }
    public void Dispose()
    {
        _signal?.TryUnsubscribe<UpdateScoreSignal>(UpdateScore);
        _signal?.TryUnsubscribe<SavePlayerScoreSignal>(SaveScore);

    }
}
