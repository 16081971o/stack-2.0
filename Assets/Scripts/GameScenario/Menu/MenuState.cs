﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace GameScenario.Menu
{
    /// <summary>
    /// Состояние главного меню
    /// </summary>
    public class MenuState : GameScenarioState
    {
        private List<IMenu> _iMenuList;

        [Inject] 
        private SignalBus _signal;

        [Inject] 
        private GameFactories _gameFactories;

        [Inject] 
        private UIManager _uiManager;
        
        public MenuState(int order, List<IMenu> menu)
        {
            this.order = order;
            _iMenuList = menu;

        }

        private OnCompleteState _onCompleteState;

        public override void Start(OnCompleteState onCompleteState)
        {
            _onCompleteState += onCompleteState;

            UpdateUI();
            foreach (var imenu in _iMenuList)
            {
                imenu.Start();
            }

            RegisterStateChangeCallback();
            CreateStartPlatform();
        }
        /// <summary>
        /// Обновляем пользовательский интерфейс
        /// </summary>
        private void UpdateUI()
        {
            _uiManager.ShowMenu(true);
            _uiManager.ShowGameplay(false);
        }
        /// <summary>
        /// Регистрируем сигнал, по которому состояние должно смениться на другое
        /// </summary>
        private void RegisterStateChangeCallback()
        {
            _signal.Fire<RegisterScenarioChangeSignal>(new RegisterScenarioChangeSignal(typeof(TouchSignal), EndStateCallback));

        }
        /// <summary>
        /// Создаем стартовую платформу
        /// </summary>
        private void CreateStartPlatform()
        {
            var Platformblock = _gameFactories.CreateStaticBlock(Vector3.one, Vector3.zero);
            _signal.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(Platformblock.gameObject,true));

        }
        public override void Update()
        {
            foreach (var imenu in _iMenuList)
            {
                imenu.Update();
            }
        }
        public void EndStateCallback(object signal)
        {
            _signal.Unsubscribe(signal.GetType(), EndStateCallback);
            End();
        }

        public override void End()
        {
            foreach (var imenu in _iMenuList)
            {
                imenu.End();
            }
            _onCompleteState();
        }
    }
}
