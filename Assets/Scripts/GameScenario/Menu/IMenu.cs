﻿/// <summary>
/// Интерфейс реализуют  механики игрового меню
/// </summary>
public interface IMenu
{
   void Start();
   void End();
   void Update();
}
