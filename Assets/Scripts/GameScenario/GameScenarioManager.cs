﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace GameScenario
{
    /// <summary>
    /// Хранит и управляет игровыми состояниями
    /// </summary>
    public class GameScenarioManager : ITickable, IInitializable
    {
        private List<GameScenarioState> gameStates;

        private GameScenarioState currentGameScenario;

        [Inject]
        public void Setup(List<GameScenarioState> list)
        {
            gameStates = list;
        }

        public void Initialize()
        {
            ChangeGameScenario(GetFirstState());
        }

        private int GetFirstState()
        {
            return gameStates.Min(item => item.order);
        }
        /// <summary>
        /// Смена состояния
        /// </summary>
        /// <param name="scenarioNumber"></param>
        public void ChangeGameScenario(int scenarioNumber)
        {
            currentGameScenario = gameStates[scenarioNumber];
            currentGameScenario.Start(StateOnComplete);
        }

        /// <summary>
        /// Колбек на завершение одного из состоянии
        /// </summary>
        public void StateOnComplete()
        {
            int newIndex = gameStates.IndexOf(currentGameScenario) + 1;

            newIndex = newIndex == gameStates.Count ? 0 : newIndex;
            
            ChangeGameScenario(newIndex);
        }

        public void Tick()
        {
            currentGameScenario.Update();
        }
    }
}
