﻿using System;
using Zenject;

namespace GameScenario
{
    /// <summary>
    /// Регистрирует сигнал после которого нужно сменить игровое состоние
    /// </summary>
    public class GameScenarioCallback : IInitializable, IDisposable
    {
        [Inject] 
        private SignalBus _registrationSignal;

        public void Initialize()
        {
            _registrationSignal?.Subscribe<RegisterScenarioChangeSignal>(RegistateCallback);
        }

        public void Dispose()
        {
            _registrationSignal?.TryUnsubscribe<RegisterScenarioChangeSignal>(RegistateCallback);
        }

        public void RegistateCallback(RegisterScenarioChangeSignal signal)
        {
            _registrationSignal?.Subscribe(signal.signalType, signal.myAction);
        }

    }
}
