﻿using UnityEngine;
using Zenject;
/// <summary>
/// Условие разделение динамического блока
/// </summary>
public class SplitBlockCondition : GameConditionState
{
    [Inject] 
    private SignalBus _splitSignal;

    public SplitBlockCondition(int priority)
    {
        base.priority = priority;
    }
    public override bool Executed(GameObject block, GameObject platform)
    {
        Vector3 blockDirection = (block.transform.position - platform.transform.position).normalized;
        Vector3 forward        = platform.transform.forward;
        float dotProduct       = Mathf.Abs(Vector3.Dot(blockDirection, forward));
        
        SplitBlockSignal.Split split = dotProduct > 0 ? SplitBlockSignal.Split.Z : SplitBlockSignal.Split.X;
        
        // Отправляем сигнал об разделение блока
        _splitSignal?.Fire<SplitBlockSignal>(new SplitBlockSignal(split,block,platform));
        
        return true;
    }
}
