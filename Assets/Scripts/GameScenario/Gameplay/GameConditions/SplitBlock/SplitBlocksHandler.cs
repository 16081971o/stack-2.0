﻿using System;
using UnityEngine;
using Zenject;
/// <summary>
/// Обработчик события об разделении блока
/// </summary>
public class SplitBlocksHandler : IInitializable, IDisposable
{
    [Inject]
    private SignalBus _signalBus;

    [Inject] 
    private GameFactories _gameFactories;
    
    [Inject]
    private PlayableBlocksHolder _blocksHolder;
    
    public void Initialize()
    {
        _signalBus?.Subscribe<SplitBlockSignal>(SplitBlock);

    }

    public void Dispose()
    {
        _signalBus?.TryUnsubscribe<SplitBlockSignal>(SplitBlock);
    }
    
    public void SplitBlock(SplitBlockSignal splitSignal)
    {
        switch (splitSignal.currentSplit)
        {
            case SplitBlockSignal.Split.X:
                SplitX(splitSignal.staticBlock, splitSignal.dynamicBlock);
                FireSignals();
                break;
            case SplitBlockSignal.Split.Z:
                SplitZ(splitSignal.staticBlock, splitSignal.dynamicBlock);
                FireSignals();
                break;
        }
    }
    
    private void FireSignals()
    {
        _signalBus.Fire<SendOnRailsSignal>(new SendOnRailsSignal());
        _signalBus.Fire<UpdateScoreSignal>(new UpdateScoreSignal());;
        _signalBus.Fire<CameraMoveSignal>(new CameraMoveSignal());
        _signalBus.Fire<PhoneVibrationSignal>(new PhoneVibrationSignal());
        
        var block        = _blocksHolder.GetPlayableDynamicBlock();
        var statiсBlock  = _blocksHolder.GetPlayableStaticBlock();
        var fallingBlock = _blocksHolder.GetPlayableFallingBlock();
        
        _signalBus.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(fallingBlock.gameObject));
        _signalBus.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(statiсBlock.gameObject,true));
        _signalBus.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(block.gameObject));
    }
    /// <summary>
    /// Разделяем блок по оси X
    /// </summary>
    /// <param name="platform"></param>
    /// <param name="block"></param>
    private void SplitX(GameObject platform, GameObject block)
    {
        Vector3 platformSize    = platform.transform.localScale;
        Vector3 blockPos        = new Vector3(block.transform.position.x,platform.transform.position.y,block.transform.position.z);
        float fallingBLockSize  = Vector3.Distance(platform.transform.position,blockPos);
        Vector3 platformPos     = platform.transform.position;
        float platformTop       = platformPos.y + platform.transform.localScale.y/2;
        float direction         = blockPos.x > platformPos.x ? 1f : -1f;
        
        Vector3 newPlatformSize  = new Vector3(platformSize.x - fallingBLockSize, block.transform.localScale.y, platformSize.z);
        Vector3 newPlatformPos   = new Vector3(platformPos.x + (direction * (fallingBLockSize/ 2f)), platformTop + newPlatformSize.y /2f , platformPos.z);

       _gameFactories.CreateStaticBlock(newPlatformSize,newPlatformPos);
       
        Vector3 fallingBlockSize = new Vector3(fallingBLockSize, block.transform.localScale.y, platformSize.z);
        Vector3 fallingBlockPos  = new Vector3(blockPos.x + (direction * (newPlatformSize.x/ 2f)), platformTop + fallingBlockSize.y /2, platformPos.z);

        _gameFactories.CreateFallingBlock(fallingBlockSize,fallingBlockPos);

    }
    // Разделяем блок по оси Z
    private void SplitZ(GameObject platform, GameObject block)
    {
        Vector3 platformSize   = platform.transform.localScale;
        Vector3 blockPos       = new Vector3(block.transform.position.x, platform.transform.position.y, block.transform.position.z);
        float fallingBLockSize = Vector3.Distance(platform.transform.position,blockPos);
        Vector3 platformPos    = platform.transform.position;
        float platformTop      = platformPos.y + platform.transform.localScale.y/2;
        float direction        = blockPos.z > platformPos.z ? 1f : -1f;
        
        Vector3 newPlatformSize  = new Vector3(platformSize.x, block.transform.localScale.y, platformSize.z - fallingBLockSize);
        Vector3 newPlatformPos   = new Vector3(platformPos.x, platformTop + newPlatformSize.y /2f,platformPos.z + (direction * (fallingBLockSize/ 2f)));

        _gameFactories.CreateStaticBlock(newPlatformSize,newPlatformPos);
        
        
        Vector3 fallingBlockSize = new Vector3(platformSize.x, block.transform.localScale.y, fallingBLockSize);
        Vector3 fallingBlockPos  = new Vector3(platformPos.x, platformTop + fallingBlockSize.y /2f, blockPos.z + (direction * (newPlatformSize.z/ 2f)));

        _gameFactories.CreateFallingBlock(fallingBlockSize,fallingBlockPos);
        
    }
}
