﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
/// <summary>
/// Условие завершения игры
/// </summary>
public class EndGameCondition : GameConditionState
{
    [Inject] 
    private SignalBus _endGameSignal;
    public EndGameCondition(int priority)
    {
        base.priority = priority;
    }
    public override bool Executed(GameObject block, GameObject platform)
    {
        bool blockWillFall = false;
        
        Vector3 blockPos = new Vector3(block.transform.position.x,platform.transform.position.y,block.transform.position.z);
        
        Vector3 positionDifference = platform.transform.position - blockPos;
        
        Vector3 scaleDifference = new Vector3(
            positionDifference.x * platform.transform.localScale.x,
            positionDifference.y * platform.transform.localScale.y,
            positionDifference.z * platform.transform.localScale.z);

        float platformScale = scaleDifference.magnitude / positionDifference.magnitude;

        blockWillFall = platformScale < positionDifference.magnitude;
        
        if (blockWillFall)
        {
            _endGameSignal?.Fire<EndGameSignal>();
            return true;
        }
        
        return false;
    }
}
