﻿using System;
using UnityEngine;
using Zenject;
/// <summary>
/// Обработчик события об идеальной установке блока
/// </summary>
public class PerfectPlaceHandler : IInitializable, IDisposable
{
    [Inject]
    private SignalBus _signalBus;

    [Inject] 
    private GameFactories _gameFactories;

    [Inject] 
    private PlayableBlocksHolder blocksHolder;
    public void Initialize()
    {
        _signalBus?.Subscribe<PerfectPlacedSignal>(PerfectPlace);
    }

    public void Dispose()
    {
        _signalBus?.TryUnsubscribe<PerfectPlacedSignal>(PerfectPlace);
    }

    private void PerfectPlace(PerfectPlacedSignal splitSignal)
    {
        GameObject platform    = splitSignal.platform;
        GameObject block       = splitSignal.block;
        float platformTop      = platform.transform.position.y + platform.transform.localScale.y / 2f;
        Vector3 newPlatformPos = new Vector3(platform.transform.position.x,platformTop + block.transform.localScale.y / 2f,platform.transform.position.z);
        
        _gameFactories.CreateStaticBlock(block.transform.localScale,newPlatformPos);

        FireSignals();
    }

    private void FireSignals()
    {
        _signalBus.Fire<SendOnRailsSignal>(new SendOnRailsSignal());
        _signalBus.Fire<UpdateScoreSignal>(new UpdateScoreSignal());
        _signalBus.Fire<CameraMoveSignal>(new CameraMoveSignal());
        _signalBus.Fire<PhoneVibrationSignal>(new PhoneVibrationSignal());
        
        var block       = blocksHolder.GetPlayableDynamicBlock();
        var statiсBlock = blocksHolder.GetPlayableStaticBlock();
        
        _signalBus.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(statiсBlock.gameObject,true));
        _signalBus.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(block.gameObject));
    }
}
