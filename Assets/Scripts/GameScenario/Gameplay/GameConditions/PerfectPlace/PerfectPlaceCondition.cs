﻿using UnityEngine;
using Zenject;

/// <summary>
/// Условие идеально поставленного блока
/// </summary>
public class PerfectPlaceCondition : GameConditionState
{
    [Inject] 
    private SignalBus _perfectPlacedSignal;
    public PerfectPlaceCondition(int priority)
    {
        base.priority = priority;
    }
    public override bool Executed(GameObject block, GameObject platform)
    {
        Vector3 blockPos           = new Vector3(block.transform.position.x, platform.transform.position.y, block.transform.position.z);
        Vector3 positionDifference =  platform.transform.position - blockPos;

        Vector3 scaleDifference    = new Vector3(
            positionDifference.x * platform.transform.localScale.x,
            positionDifference.y * platform.transform.localScale.y,
            positionDifference.z * platform.transform.localScale.z);

        float platformScale        = scaleDifference.magnitude / positionDifference.magnitude;

        bool blockPlacePerfectly   =  platformScale * 0.08f > positionDifference.magnitude;
        
        if (blockPlacePerfectly)
        {
            // Отправляем событие об идеальной установке блока
            _perfectPlacedSignal?.Fire<PerfectPlacedSignal>(new PerfectPlacedSignal(platform,block));
            return true;
        }

        return false;
    }
}
