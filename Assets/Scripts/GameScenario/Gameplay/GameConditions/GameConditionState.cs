﻿using UnityEngine;
/// <summary>
/// Представляение о событиях в игре
/// </summary>
public abstract class GameConditionState 
{
    // приоритетность выполнения игрового события
    public int priority;
    // метод выполняющий действие определенное игровым событием
    public abstract bool Executed(GameObject block, GameObject platform);
}
