﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameScenario.GamePlay;
using UnityEngine;
using Zenject;

namespace GameScenarioManager.GamePlay
{
    /// <summary>
    /// Обработчик игровых событий
    /// </summary>
    public class GameConditionsManager : IDisposable, IGameplay
    {
        [Inject] private SignalBus _touchEvent;

        private List<GameConditionState> _conditionsStates;

        [Inject] private PlayableBlocksHolder _playableBlocksHolder;

        [Inject]
        public void Setup(List<GameConditionState> conditionsStates)
        {
            _conditionsStates = conditionsStates;
        }

        public void Start()
        {
            SubscribeToSignals();
        }

        private void SubscribeToSignals()
        {
            _touchEvent?.Subscribe<TouchSignal>(Deside);
        }

        private void UnsubcribeToSignal()
        {
            _touchEvent?.TryUnsubscribe<TouchSignal>(Deside);
        }

        public void Update()
        {
        }

        public void End()
        {
            UnsubcribeToSignal();
        }

        public void Dispose()
        {
            UnsubcribeToSignal();
        }

        /// <summary>
        /// Сортируем условия по приоритету
        /// </summary>
        private void SortConditionsbyPriority()
        {
            _conditionsStates = _conditionsStates.OrderBy(item => item.priority).ToList();
        }

        private void Deside()
        {
            SortConditionsbyPriority();

            MonoBehaviour block = _playableBlocksHolder.GetPlayableDynamicBlock();
            MonoBehaviour platform = _playableBlocksHolder.GetPlayableStaticBlock();

            foreach (GameConditionState state in _conditionsStates)
            {
                bool isDone = state.Executed(block.gameObject, platform.gameObject);

                if (isDone) break;
            }
        }
    }
}
