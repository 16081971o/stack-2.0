﻿namespace GameScenario.GamePlay
{
    /// <summary>
    /// Инртерфейс который реализуют игровые механики
    /// </summary>
    public interface IGameplay
    {
        void Start();
        void End();
        void Update();
    }
}