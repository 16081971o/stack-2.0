﻿using System;
using UnityEngine;
using Zenject;
/// <summary>
/// Окрашивет блок в цвет градиента
/// </summary>
public class ColorizeBlockHandler : IInitializable, IDisposable
{
    [Inject]
    private SignalBus _signalBus;

    [Inject] 
    private GameFactories _gameFactories;

    private Color _firstColor;
    private Color _secondColor;
    private Color _currentColor;
    
    private void RandomColors()
    {
        _firstColor = UnityEngine.Random.ColorHSV();
        _secondColor = UnityEngine.Random.ColorHSV();
        _currentColor = Color.Lerp(_firstColor,_secondColor,0.25f);
    }
    public void Initialize()
    {
        RandomColors();
        _signalBus?.Subscribe<ColorizeBlockSignal>(Colorize);
    }

    public void Dispose()
    {
        _signalBus?.TryUnsubscribe<ColorizeBlockSignal>(Colorize);
    }


    public void Colorize(ColorizeBlockSignal signalBus)
    {
        MeshRenderer mesh = signalBus.block.GetComponent<MeshRenderer>();
        mesh.material.color = _currentColor;

        if (signalBus.changeColor)
        {
            MakeColorStep();
        }

        bool needNewColor = Mathf.Abs(_currentColor.r - _secondColor.r) < .1f &&
                           Mathf.Abs(_currentColor.b - _secondColor.b) < .1f &&
                           Mathf.Abs(_currentColor.g - _secondColor.g) < .1f;

        if (needNewColor)
        {
            RandomColors();
        }

    }
    private void MakeColorStep()
    {
        _currentColor = Color.Lerp(_currentColor,_secondColor,0.25f);
    }
}
