﻿using System;
using System.Collections.Generic;
using GameScenario.GamePlay;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace GameScenarioManager.GamePlay
{
    /// <summary>
    /// Отвечает за перемещение IDynamic
    /// </summary>
    public class BlockRailsHandler : IDisposable, IGameplay
    {
        [Inject] private PlayableBlocksHolder _playableBlocksHolder;

        [Inject] private SignalBus _sendOnRailsSignal;

        [Inject] private GameSettings _gameSettings;
        
        private float distance = 2;
        private float speed = 2f;
        private Queue<Vector3> moveList;

        private List<Vector3> directions = new List<Vector3>()
        {
            Vector3.forward,
            Vector3.back,
            Vector3.right,
            Vector3.left
        };

        public void Start()
        {
            distance = _gameSettings.distanceFromPlatform;
            speed   = _gameSettings.blockMovingSpeed;
            SubscribeToSignals();
        }

        private void SubscribeToSignals()
        {
            _sendOnRailsSignal?.Subscribe<SendOnRailsSignal>(OnRails);
        }

        private void UnsubcribeToSignal()
        {
            _sendOnRailsSignal?.TryUnsubscribe<SendOnRailsSignal>(OnRails);

        }
        public void Update()
        {
            MoveOnRails();
        }

        public void End()
        {
            UnsubcribeToSignal();
        }

        public void Dispose()
        {
            UnsubcribeToSignal();
        }

        private void MoveOnRails()
        {
            MonoBehaviour playableBlock = _playableBlocksHolder.GetPlayableDynamicBlock();

            playableBlock.transform.position = Vector3.MoveTowards(playableBlock.transform.position, moveList.Peek(),
                Time.deltaTime * speed);

            if (ReceiveFinalPoint(playableBlock.transform))
            {
                Vector3 finishPos = moveList.Dequeue();
                moveList.Enqueue(finishPos);
            }

        }

        private bool ReceiveFinalPoint(Transform transform)
        {
            return transform.position == moveList.Peek();
        }

        private Vector3 PickRail(Transform platfromTransform, Transform blockTransform)
        {
            float blockSize = blockTransform.transform.localScale.y;
            float platformTop = platfromTransform.position.y + platfromTransform.localScale.y / 2;
            Vector3 blockCentre = new Vector3(platfromTransform.transform.position.x, platformTop + blockSize / 2,
                platfromTransform.transform.position.z);

            Vector3 randomDirection = directions[Random.Range(0, directions.Count)];
            Vector3 startPos = blockCentre + randomDirection * distance;
            Vector3 finish = blockCentre + randomDirection * distance * -1f;

            BuidBlockRail(finish, startPos);

            return startPos;
        }

        private void BuidBlockRail(Vector3 finish, Vector3 startPos)
        {
            moveList = new Queue<Vector3>();
            moveList.Enqueue(finish);
            moveList.Enqueue(startPos);
        }

        private void OnRails(SendOnRailsSignal sendOnRails)
        {
            MonoBehaviour playableBlock = _playableBlocksHolder.GetPlayableDynamicBlock();

            MonoBehaviour platformBlock = _playableBlocksHolder.GetPlayableStaticBlock();

            playableBlock.transform.localScale = new Vector3(platformBlock.transform.localScale.x,
                playableBlock.transform.localScale.y, platformBlock.transform.localScale.z);
            playableBlock.transform.position = PickRail(platformBlock.transform, playableBlock.transform);

        }
    }
}
