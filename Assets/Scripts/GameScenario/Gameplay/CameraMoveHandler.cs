﻿using System;
using GameScenario.GamePlay;
using UnityEngine;
using Zenject;
namespace GameScenario.GamePlay
{
    /// <summary>
    /// Обрабатывает передвижение камеры
    /// </summary>
    public class CameraMoveHandler : IDisposable, IGameplay
    {
        [Inject] private SignalBus _cameraMoveSignal;

        [Inject] private PlayableBlocksHolder _playableBlocksHolder;

        private Vector3 nextPos = Vector3.zero;

        public void Start()
        {
            SubscribeToSignals();
        }

        private void SubscribeToSignals()
        {
            _cameraMoveSignal?.Subscribe<CameraMoveSignal>(CalculateNewPosition);
        }

        private void UnsubcribeToSignal()
        {
            _cameraMoveSignal?.TryUnsubscribe<CameraMoveSignal>(CalculateNewPosition);
        }

        public void Update()
        {
            Move();
        }

        public void Move()
        {
            if (Camera.main.transform.position != nextPos && MoveCondition())
            {
                Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, nextPos, Time.deltaTime * 0.45f);
            }
        }

        private bool MoveCondition()
        {
            return _playableBlocksHolder?.StaticBlocks.Count > 3;
        }

        public void Dispose()
        {
            UnsubcribeToSignal();
        }

        public void End()
        {
            UnsubcribeToSignal();
        }

        public void CalculateNewPosition()
        {
            var block = _playableBlocksHolder.GetPlayableDynamicBlock();
            nextPos = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y + block.transform.localScale.y, Camera.main.transform.position.z);
        }
    }
}
