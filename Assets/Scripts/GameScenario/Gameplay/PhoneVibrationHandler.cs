﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameScenario.GamePlay;
using RDG;
using UnityEngine;
using Zenject;
/// <summary>
/// Обработчик вибрации после установки блока
/// </summary>
public class PhoneVibrationHandler : IGameplay,IDisposable
{
    [Inject] private SignalBus signal;
    
    public void Start()
    {
        SubscribeToSignals();
    }
    private void SubscribeToSignals()
    {
        signal?.Subscribe<PhoneVibrationSignal>(Vibrate);
    }
    private void UnsubcribeToSignal()
    {
        signal?.TryUnsubscribe<PhoneVibrationSignal>(Vibrate);
    }

    public void Dispose()
    {
        UnsubcribeToSignal();
    }

    public void End()
    {
        UnsubcribeToSignal();
    }

    public void Update() { }

    private void Vibrate()
    {
#if UNITY_ANDROID
        Vibration.VibratePredefined(Vibration.PredefinedEffect.EFFECT_TICK);
        Vibration.Vibrate(50);
#endif
#if !UNITY_ANDROID
         if (Vibration.HasVibrator())
         {
             Vibration.VibratePredefined(Vibration.PredefinedEffect.EFFECT_TICK);
             Vibration.Vibrate(50);
         }
         else
         {
             Handheld.Vibrate();
         } 
#endif
    }
}
