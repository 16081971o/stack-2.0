﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace GameScenario.GamePlay
{
    /// <summary>
    /// Состояние игрового режима приложения
    /// </summary>
    public class GamePlayState : GameScenarioState
    {
        private List<IGameplay> _igameplaysList;
        private OnCompleteState _onCompleteState;

        [Inject] 
        private GameFactories factory;
        
        [Inject] 
        private SignalBus _signal;

        [Inject] 
        private UIManager _uiManager;
        
        public GamePlayState(int order, List<IGameplay> gameplays)
        {
            this.order = order;
            _igameplaysList = gameplays;
        }

        public override void Start(OnCompleteState onCompleteState)
        {
            _onCompleteState = onCompleteState;
            foreach (var igameplay in _igameplaysList)
            {
                igameplay.Start();
            }

            UpdateUI();

            CreateFirstDynamic();

            RegisterStateChangeCallback();
        }
        /// <summary>
        /// Обновляем пользовательский интерфейс
        /// </summary>
        private void UpdateUI()
        {
            _uiManager.ShowGameplay(true);
            _uiManager.ShowMenu(false);
        }
        /// <summary>
        /// Создаем первый динамический блок
        /// </summary>
        private void CreateFirstDynamic()
        {
            var block = factory.CreateDynamicBlock(new Vector3(1, 0.2f, 1f), Vector3.zero);
            _signal.Fire<SendOnRailsSignal>(new SendOnRailsSignal());
            _signal.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(block.gameObject));

        }
        /// <summary>
        /// Регистрируем сигнал, по которому состояние должно смениться на другое
        /// </summary>
        private void RegisterStateChangeCallback()
        {
            _signal.Fire<RegisterScenarioChangeSignal>(new RegisterScenarioChangeSignal(typeof(EndGameSignal), EndStateCallback));
        }

        public void EndStateCallback(object signal)
        {
            _signal.Unsubscribe(signal.GetType(), EndStateCallback);
            End();
        }

        public override void Update()
        {
            foreach (var igameplay in _igameplaysList)
            {
                igameplay.Update();
            }
        }

        public override void End()
        {
            foreach (var igameplay in _igameplaysList)
            {
                igameplay.End();
            }
            _onCompleteState();
        }
    }
}