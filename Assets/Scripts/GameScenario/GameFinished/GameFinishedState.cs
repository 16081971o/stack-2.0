﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
namespace GameScenario.GameFinished
{
    /// <summary>
    /// Состояние которая работает после проигрыша игрока
    /// </summary>
    public class GameFinishedState : GameScenarioState
    {
        private List<IGameFinished> _iEndGameList;
        private OnCompleteState _onCompleteState;

        [Inject] 
        private SignalBus _signal;

        [Inject] 
        private UIManager _uiManager;
        
        [Inject] 
        private PlayableBlocksHolder _playableBlocksHolder;
        
        [Inject]
        private GameFactories _gameFactories;
        
        public GameFinishedState(int order, List<IGameFinished> iEndGameList)
        {
            this.order = order;
            _iEndGameList = iEndGameList;

        }

        public override void Start(OnCompleteState onCompleteState)
        {
            _onCompleteState = onCompleteState;

            UpdateUI();
            
            foreach (var iEndGame in _iEndGameList)
            {
                iEndGame.Start();
            }

            FallDownBLock();
            
            FireFinishGameSignals();

        }
        /// <summary>
        /// Отправляем сигналы данного состояния
        /// </summary>
        private void FireFinishGameSignals()
        {
            _signal.Fire<CameraChinematicMoveSignal>();
            _signal.Fire<SavePlayerScoreSignal>();
        }
        /// <summary>
        /// 
        /// </summary>
        private void FallDownBLock()
        {
            MonoBehaviour dynamic = _playableBlocksHolder.GetPlayableDynamicBlock();
            Vector3 scale         = dynamic.transform.localScale;
            Vector3 position      = dynamic.transform.position;
            var fallingBlock      = _gameFactories.CreateFallingBlock(scale, position);
            
            _signal.Fire<ColorizeBlockSignal>(new ColorizeBlockSignal(fallingBlock.gameObject));
            
            MonoBehaviour.Destroy(dynamic.gameObject);
        }
        /// <summary>
        /// Обновляем пользовательский интерфейс
        /// </summary>
        private void UpdateUI()
        {
            _uiManager.ChangeTitle("Tap to restart");
            _uiManager.ShowMenu(true);
        }
        public override void Update()
        {
            foreach (var iEndGame in _iEndGameList)
            {
                iEndGame.Update();
            }
        }

        public void EndStateCallback(object signal)
        {
            _signal.Unsubscribe(signal.GetType(), EndStateCallback);
            End();
        }

        public override void End()
        {
            foreach (var iEndGame in _iEndGameList)
            {
                iEndGame.End();
            }
            _onCompleteState();
        }
    }
}
