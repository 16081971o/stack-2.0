﻿using System;
using System.Collections.Generic;
using GameScenario.GameFinished;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using Random = UnityEngine.Random;
//TODO: Создан только в рамках отсутствия времени - правильный подход это цикличная работа состояний GameScenarioManager
/// <summary>
/// Отвечает за перезапуск сцены
/// </summary>
public class RestartGameHandler : IGameFinished, IDisposable
{
    [Inject] 
    private SignalBus _touchSignal;

    [Inject] 
    private PlayableBlocksHolder _playableBlocksHolder;

    private float finishTime;
    private bool Restart;
    
    public void Start()
    {
        SubscribeToSignals();
    }
    
    private void SubscribeToSignals()
    {
        _touchSignal?.Subscribe<TouchSignal>(RestartHandler);
    }

    private void UnsubcribeToSignal()
    {
        _touchSignal?.TryUnsubscribe<TouchSignal>(RestartHandler);
    }

    private void RestartHandler()
    {
        CrashBlocks();
        finishTime = Time.time + 1.2f;
        Restart    = true;
        UnsubcribeToSignal();
    }

    private void CrashBlocks()
    {
        List<Vector3> list = new List<Vector3>()
        {
            Vector3.down,
            Vector3.up,
            Vector3.left,
            Vector3.right
        };
        foreach (var staticBlock in _playableBlocksHolder.StaticBlocks)
        {
            MonoBehaviour platfrom = staticBlock as MonoBehaviour;
            platfrom.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            platfrom.gameObject.GetComponent<Rigidbody>().AddForce(list[Random.Range(0,list.Count)] * 500f);
        }
    }
    public void Update()
    {
        if (Restart && Time.time > finishTime)
        {
            SceneManager.LoadScene("GameScene");
        }
    }
    public void End()
    {
        UnsubcribeToSignal();
    }

    public void Dispose()
    {
        UnsubcribeToSignal();
    }
}
