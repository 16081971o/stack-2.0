﻿namespace GameScenario.GameFinished
{
    /// <summary>
    /// Интерфейс реализуют механики конца игры 
    /// </summary>
    public interface IGameFinished
    {
        /// <summary>
        /// Метод вызывается в начале работы 
        /// </summary>
        void Start();
        /// <summary>
        /// Метод вызывается в конце работы 
        /// </summary>
        void End();
        /// <summary>
        /// Метод вызывается каждый фрейм
        /// </summary>
        void Update();
    }
}