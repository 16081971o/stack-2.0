﻿using System;
using GameScenario.GameFinished;
using UnityEngine;
using Zenject;

namespace GameScenario.GameFinished
{
    /// <summary>
    /// Отвечает за плавное отведение камеры от нашей башни
    /// </summary>
    public class CameraChinematicMoveHandler : IGameFinished, IDisposable
    {
        [Inject] private SignalBus _cameraMoveSignal;

        [Inject] private PlayableBlocksHolder _playableBlocksHolder;

        private float speed;
        private float cameraRemoveDistance;

        public void Start()
        {
            SubscribeToSignals();
        }

        private void SubscribeToSignals()
        {
            _cameraMoveSignal?.Subscribe<CameraChinematicMoveSignal>(CalculateCinematicStep);
        }

        private void UnsubcribeToSignal()
        {
            _cameraMoveSignal?.TryUnsubscribe<CameraChinematicMoveSignal>(CalculateCinematicStep);
        }

        public void Update()
        {
            if (Camera.main.orthographicSize <= cameraRemoveDistance)
            {
                Camera.main.orthographicSize += Time.deltaTime * speed;
            }
        }

        public void End()
        {
            UnsubcribeToSignal();
        }

        public void CalculateCinematicStep()
        {
            int blocks = _playableBlocksHolder.StaticBlocks.Count - 1;
            float newSize = ((blocks * 0.2f) + 1f) * Camera.main.orthographicSize / 2f;
            cameraRemoveDistance = speed = newSize;
        }

        public void Dispose()
        {
            UnsubcribeToSignal();
        }
    }
}
