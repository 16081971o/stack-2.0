﻿using UnityEngine;
/// <summary>
/// Игровые настройки
/// </summary>
[CreateAssetMenu(fileName = "GameSettings",menuName = "Create GameSetting")]
public class GameSettings : ScriptableObject
{
    public GameObject dynamicBlock;
    public GameObject staticBlock;
    public GameObject fallingBlock;
    public float blockMovingSpeed;
    public float distanceFromPlatform;
}
