﻿using UnityEngine;
using Zenject;
[CreateAssetMenu(fileName ="SoInstaller", menuName = "Create SO Installer")]
public class SoInstaller : ScriptableObjectInstaller
{
    [SerializeField] 
    private GameSettings _gameSettings;
    public override void InstallBindings()
    {
        Container.BindInstance(_gameSettings);
    }
}
