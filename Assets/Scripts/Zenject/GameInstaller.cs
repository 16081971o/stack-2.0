﻿using GameScenario;
using GameScenario.GameFinished;
using GameScenario.GamePlay;
using GameScenario.Menu;
using GameScenarioManager.GamePlay;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [Inject] 
    private GameSettings _gameSettings;
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<GameScenarioCallback>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameScenario.GameScenarioManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameConditionsManager>().AsSingle();
        Container.Bind<GameConditionState>().To<EndGameCondition>().AsSingle().WithArguments(0);
        Container.Bind<GameConditionState>().To<PerfectPlaceCondition>().AsSingle().WithArguments(1);
        Container.Bind<GameConditionState>().To<SplitBlockCondition>().AsSingle().WithArguments(2);


        Container.Bind<GameScenarioState>().To<MenuState>().AsSingle().WithArguments(0);
        Container.Bind<GameScenarioState>().To<GamePlayState>().AsSingle().WithArguments(1);
        Container.Bind<GameScenarioState>().To<GameFinishedState>().AsSingle().WithArguments(2);

        Container.BindInterfacesAndSelfTo<CameraMoveHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<CameraChinematicMoveHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<RestartGameHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<PerfectPlaceHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<SplitBlocksHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<BlockRailsHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<UserTouchHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<PhoneVibrationHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<ColorizeBlockHandler>().AsSingle();
        Container.Bind<PlayableBlocksHolder>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerScore>().AsSingle();
        Container.BindExecutionOrder<ColorizeBlockHandler>(-1);

        BindFactories();
        BindSignals();
    }
    void BindSignals()
    {
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<TouchSignal>();
        Container.DeclareSignal<CameraMoveSignal>();
        Container.DeclareSignal<SplitBlockSignal>();
        Container.DeclareSignal<PerfectPlacedSignal>();
        Container.DeclareSignal<EndGameSignal>();
        Container.DeclareSignal<SendOnRailsSignal>();
        Container.DeclareSignal<UpdateScoreSignal>();
        Container.DeclareSignal<CameraChinematicMoveSignal>();
        Container.DeclareSignal<RegisterScenarioChangeSignal>();
        Container.DeclareSignal<SavePlayerScoreSignal>();
        Container.DeclareSignal<PhoneVibrationSignal>();
        Container.DeclareSignal<ColorizeBlockSignal>();

    }

    void BindFactories()
    {
        Container.BindFactory<Vector3, Vector3, Block, Block.Factory>().FromComponentInNewPrefab(_gameSettings.dynamicBlock);
        Container.BindFactory<Vector3,Vector3,FallingBlock, FallingBlock.Factory>().FromComponentInNewPrefab(_gameSettings.fallingBlock);
        Container.BindFactory<Vector3,Vector3,PlatformBlock, PlatformBlock.Factory>().FromComponentInNewPrefab(_gameSettings.staticBlock);
        Container.Bind<GameFactories>().AsSingle();

    }
}
