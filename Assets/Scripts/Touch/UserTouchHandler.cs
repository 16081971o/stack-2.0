﻿using UnityEngine;
using Zenject;
/// <summary>
/// Отлавливает нажатие пользователя на экран
/// </summary>
public class UserTouchHandler : ITickable
{
    [Inject] 
    private SignalBus _touchSignal;
    
    public void Tick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _touchSignal.Fire<TouchSignal>();
        }
    }
}
