﻿using UnityEngine;
/// <summary>
/// При помощи этого сигнала окрашиваються блоки
/// </summary>
public class ColorizeBlockSignal
{
   public GameObject block;
   public bool changeColor;
   public ColorizeBlockSignal(GameObject block, bool changeColor = false)
   {
      this.block       = block;
      this.changeColor = changeColor;
   }
}
