﻿using System;
/// <summary>
/// Игровой режим регистрирует сигнал который сменит игровое состояние
/// </summary>
public class RegisterScenarioChangeSignal
{
   public Action<object> myAction;
   public Type signalType;

   public RegisterScenarioChangeSignal(Type signalType, Action<object> myAction)
   {
      this.signalType = signalType;
      this.myAction = myAction;
   }
}
