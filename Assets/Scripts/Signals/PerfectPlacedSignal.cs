﻿using UnityEngine;

/// <summary>
/// Сигнал оповещающий об идеально поставленном блоке
/// </summary>
public class PerfectPlacedSignal
{
    public GameObject platform;
    public GameObject block;
    public PerfectPlacedSignal(GameObject platform,GameObject block)
    {
        this.platform = platform;
        this.block = block;
    }
}
