﻿using UnityEngine;
/// <summary>
/// Сигнал оповещающий об разделение текущего игрового блока (IDynamicBlock)
/// </summary>
public class SplitBlockSignal
{
    public Split currentSplit;
    public GameObject dynamicBlock;
    public GameObject staticBlock;
    public SplitBlockSignal(Split split, GameObject dynamicBlock, GameObject staticBlock)
    {
        this.currentSplit = split;
        this.dynamicBlock = dynamicBlock;
        this.staticBlock  = staticBlock;
    }
    public enum Split
    {
        X,
        Z
    }
}
