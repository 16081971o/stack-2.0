﻿using UnityEngine;
using Zenject;
/// <summary>
/// Создатель игровых сущностей
/// </summary>
public class GameFactories
{
    private PlatformBlock.Factory _staticBlockFactory;
    private FallingBlock.Factory _fallingBlockFactory;
    private Block.Factory _dynamicBlockFactory;
    
    [Inject] 
    private PlayableBlocksHolder _playableBlocksHolder;

    [Inject]
    private void Setup(Block.Factory blockFactory, PlatformBlock.Factory  staticBlockFactory, FallingBlock.Factory fallingBlockFactory)
    {
        _dynamicBlockFactory = blockFactory;
        _staticBlockFactory = staticBlockFactory;
        _fallingBlockFactory = fallingBlockFactory;
    }
    
    /// <summary>
    /// Создаем блок-платформу
    /// </summary>
    /// <returns></returns>
    public GameObject CreateStaticBlock(Vector3 scale,Vector3 position)
    {
        var staticBlock = _staticBlockFactory.Create(scale, position);
        
        //обновляем холдера
        _playableBlocksHolder.StaticBlock = staticBlock;
        _playableBlocksHolder.StaticBlocks.Add(staticBlock);
        
        return staticBlock.gameObject;
    }
    /// <summary>
    /// Создаем падающий блок
    /// </summary>
    /// <returns></returns>
    public GameObject CreateFallingBlock(Vector3 scale , Vector3 position)
    {
        var fallingBlock = _fallingBlockFactory.Create(scale, position);
        
        //обновляем холдер 
        _playableBlocksHolder.FallingBlock = fallingBlock;
        
        return fallingBlock.gameObject;
    }
    /// <summary>
    /// Создаем динамический блок
    /// </summary>
    /// <returns></returns>
    public GameObject CreateDynamicBlock(Vector3 scale, Vector3 position)
    {
        var dynamicBlock = _dynamicBlockFactory.Create(scale, position);
        
        //обновляем холдер 
        _playableBlocksHolder.DynamicBlock = dynamicBlock;
        
        return dynamicBlock.gameObject;
    }
}
